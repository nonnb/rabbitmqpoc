﻿using System;

namespace RabbitProofOfConcept
{
    public interface IBusSerializer
    {
        string MimeContentType { get; }
        byte[] Serialize(object objectToSerialize);
        T Deserialize<T>(byte[] serializedBytes);
        object Deserialize(Type type, byte[] serializedBytes);
    }
}