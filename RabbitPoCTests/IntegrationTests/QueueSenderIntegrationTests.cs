﻿using System;
using NUnit.Framework;
using RabbitProofOfConcept;

namespace RabbitPoCTests.IntegrationTests
{
    [TestFixture]
    class QueueSenderIntegrationTests
    {
        [Test]
        public void EnsureParentAndChildSerialized()
        {
            var message = new ParentCommandRequest
            {
                Id = 1234,
                SomeNaturalKey = "NaturalKey1234",
                Children = new[]
                {
                  new ChildDto
                  {
                      Id = 9876,
                      Name = "Child DTO 9876",
                      SomeNaturalKey = "ChildDTO9876",
                      SomeAmount = 567.89m,
                      SomeCurrency = SomeCurrency.ZAR,
                      SomeBoolean = true,
                      SomeDate = new DateTime(2015,11,12),
                      SomeGuid = Guid.Empty,
                  }  
                }
            };
            var sender = new RabbitSender(ObjectMothers.TestSettings, new NewtonSoftJsonSerializer(), ObjectMothers.IntegrationTestPublications);
            sender.Send(message);
        }

        [Test]
        public void EnsureSendingUnregisteredTypeThrows()
        {
            var message = new ChildDto
                  {
                      Id = 9876,
                      Name = "Child DTO 9876",
                      SomeNaturalKey = "ChildDTO9876",
                      SomeAmount = 567.89m,
                      SomeCurrency = SomeCurrency.ZAR,
                      SomeBoolean = true,
                      SomeDate = new DateTime(2015, 11, 12),
                      SomeGuid = Guid.Empty,
                  };
            using (var sender = new RabbitSender(ObjectMothers.TestSettings, new NewtonSoftJsonSerializer(), ObjectMothers.IntegrationTestPublications))
            {
                Assert.Throws<Exception>(() => sender.Send(message));
            }
        }


        [Test]
        public void EnsureDeadLetterQueueWithBadRoutingKey()
        {
            var message = new ParentCommandRequest
            {
                Id = 1234,
                SomeNaturalKey = "NaturalKey1234",
                Children = new[]
                {
                  new ChildDto
                  {
                      Id = 9876,
                      Name = "Child DTO 9876",
                      SomeNaturalKey = "ChildDTO9876",
                      SomeAmount = 567.89m,
                      SomeCurrency = SomeCurrency.ZAR,
                      SomeBoolean = true,
                      SomeDate = new DateTime(2015,11,12),
                      SomeGuid = Guid.Empty,
                  }  
                }
            };
            var sender = new RabbitSender(ObjectMothers.TestSettings, new NewtonSoftJsonSerializer(), ObjectMothers.IntegrationTestPublications);
            sender.Send(message);
        }
    }
}
