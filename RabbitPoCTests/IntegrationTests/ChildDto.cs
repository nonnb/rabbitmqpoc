﻿using System;

namespace RabbitPoCTests.IntegrationTests
{
    public class ChildDto
    {
        public int Id { get; set; }
        public Guid SomeGuid { get; set; }
        public string Name { get; set; }
        public string SomeNaturalKey { get; set; }
        public DateTime SomeDate { get; set; }
        public Decimal SomeAmount { get; set; }
        public SomeCurrency SomeCurrency { get; set; }
        public bool SomeBoolean { get; set; }
    }
}