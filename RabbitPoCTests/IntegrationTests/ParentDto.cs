﻿using System.Collections.Generic;
using MediatR;
using RainFin.BiApi.CoreServices.Messages;

namespace RabbitPoCTests.IntegrationTests
{
    public class ParentCommandResponse
    {
        public string Name { get; set; }
    }
    
    public class ParentCommandRequest : IRequest<ActionResult<ParentCommandResponse>>
    {
        public int Id { get; set; }
        public string SomeNaturalKey { get; set; }
        public IEnumerable<ChildDto> Children { get; set; }
    }
}