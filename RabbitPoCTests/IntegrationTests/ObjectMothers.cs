﻿using System;
using System.Collections.Generic;
using RabbitProofOfConcept;

namespace RabbitPoCTests.IntegrationTests
{
    internal class ObjectMothers
    {
        public static readonly RabbitMqSettings TestSettings = new RabbitMqSettings
        {
            HostName = "localhost",
            UserName = "guest",
            Password = "guest",
            Port = 5672,
            VirtualHost = "/",
            PublishExchange = "RainFin.X.Test",
            ReceiveQueue = "RainFin.Q.TargetSystem",
            ReceiveTimeoutMilliseconds = 5000,
        };

        // TODO : Consider the AG model as well, viz Dictionary of permitted Response messages for an Incoming Request
        public static readonly IDictionary<Type, string> IntegrationTestPublications = new Dictionary<Type, string>
        {
            {
                typeof (ParentCommandRequest), "RabbitPoCTests.IntegrationTests.ParentCommandRequest"
            }
        };
    }
}
