﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using MediatR;
using Moq;
using NUnit.Framework;
using RabbitProofOfConcept;
using RainFin.BiApi.CoreServices.Messages;

namespace RabbitPoCTests.IntegrationTests
{
    [TestFixture]
    public class QueueReceiveAndDispatchIntegrationTests
    {
        [Test]
        public void EnsureThatPumpDispatches()
        {
            var messageMappings = new []
                {
                    new ReceiveMessageMapping
                    {
                        CommandRequestType = typeof(ParentCommandRequest),
                        DispatchType = DispatchType.Send,
                        MessageType = "RabbitPoCTests.IntegrationTests.ParentCommandRequest"
                    }
            }
            .ToDictionary(x => x.MessageType);

            var mockLogger = new Mock<ILogger>();
            var mockClock = new Mock<IClock>();
            var mockQueueSender = new Mock<IQueueSender>();
            var mockMediator = new Mock<IMediator>();
            var receiver = new RabbitQueueReceiver(ObjectMothers.TestSettings, mockLogger.Object, mockClock.Object);
            var dispatcher = new QueueDispatcher(receiver, mockQueueSender.Object, messageMappings, new NewtonSoftJsonSerializer(),mockMediator.Object, mockLogger.Object);

            receiver.Start();
            Thread.Sleep(100000);
            receiver.Stop();
        }
    }
}
