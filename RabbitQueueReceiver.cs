﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Reflection;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.MessagePatterns;

namespace RabbitProofOfConcept
{
    public interface ILogger
    {
        // TODO : Need to be able to register context in the Logger (viz, new key value pairs which get added to all messages)
        // The AG pattern of using TLS and nested using scopes didn't quite work IMO
        void AddContext(string additionalContext);
        void AddContext(string key, string value);

        void Info(string itemToTrace);
        void Info(string formatString, params object[] arguments);
        void Error(string formatString, params object[] arguments);
        void Error(Exception ex, string formatString, params object[] arguments);
    }

    public interface IClock
    {
        DateTime Now { get; }
    }

    public class RabbitQueueReceiver : IQueueReceiver
    {
        private readonly RabbitMqSettings _settings;
        private readonly ICollection<Action<byte[], MessageMetaData>> _callbacks = new List<Action<byte[], MessageMetaData>>();
        private readonly ILogger _logger;
        private readonly IClock _clock;
        private volatile bool _stopListener = false;

        // TODO Common Toolbelt?
        public RabbitQueueReceiver(RabbitMqSettings settings, ILogger logger, IClock clock)
        {
            Contract.Requires(settings != null);
            Contract.Requires(logger != null);
            
            _settings = settings;
            _logger = logger;
            _clock = clock;
        }

        public void RegisterMessageReceivedCallback(Action<byte[], MessageMetaData> callBack)
        {
            _logger.Info("New Callback Registered: {0}", callBack.GetMethodInfo().Name);
            _callbacks.Add(callBack);
        }

        internal MessageMetaData MapBasicPropertiesToMessageMetaData(IBasicProperties basicProperties)
        {
            return new MessageMetaData
            {
                MessageType = basicProperties.Type,
                ApplicationId = basicProperties.AppId,
                ContentEncoding = basicProperties.ContentEncoding,
                // MimeContentType = basicProperties.ContentType, From Serializer
                CorrelationId = basicProperties.CorrelationId,
                Headers = basicProperties.Headers,
                MessageId = basicProperties.MessageId,
                Priority = basicProperties.Priority,
                Creation = new DateTime(basicProperties.Timestamp.UnixTime)
            };
        }

        private void MessagePump()
        {
            var factory = new ConnectionFactory
            {
                HostName = _settings.HostName,
                Password = _settings.Password,
                UserName = _settings.UserName,
                Port = _settings.Port,
                VirtualHost = _settings.VirtualHost,
                RequestedHeartbeat = 30
            };

            //  TODO : Might look here for more Robustness http://stackoverflow.com/questions/12499174/rabbitmq-c-sharp-driver-stops-receiving-messages
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            using (var subscription = new Subscription(channel, _settings.ReceiveQueue, true))
            {
                while (!_stopListener)
                {
                    BasicDeliverEventArgs message;
                    if (subscription.Next(_settings.ReceiveTimeoutMilliseconds, out message))
                    {
                        try
                        {
                            _logger.Info("Message Received: {0:yyyy-MM-dd HH:mm:ss}-{1}",
                                _clock.Now, message.BasicProperties.Type);

                            var metaData = MapBasicPropertiesToMessageMetaData(message.BasicProperties);
                            // TODO : Do we really want multiple subscribers
                            foreach (var callback in _callbacks)
                            {
                                callback(message.Body, metaData);
                            }
                            subscription.Ack(message);
                        }
                        catch (Exception ex)
                        {
                            // TODO - we can unpack result - i.e. Null, not null, some basicproperties perhaps?
                            _logger.Error(ex, "Unexpected Error in primary Rabbit message Dequeue");
                        }
                    }
                }
                _logger.Info("Queue Pump Stopped: {0:yyyy-MM-dd HH:mm:ss}", _clock.Now);
            }
        }

        public void Start()
        {
            _logger.Info("Start Received: {0:yyyy-MM-dd HH:mm:ss}", _clock.Now);
            Task.Run(() => MessagePump());
        }

        public void Stop()
        {
            _stopListener = true;
            _logger.Info("Stop Received: {0:yyyy-MM-dd HH:mm:ss}", _clock.Now);
        }
    }
}
