using System;
using System.Text;
using Newtonsoft.Json;

namespace RabbitProofOfConcept
{
    public class NewtonSoftJsonSerializer : IBusSerializer
    {
        public byte[] Serialize(object objectToSerialize)
        {
            return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(objectToSerialize));
        }

        public T Deserialize<T>(byte[] serializedBytes)
        {
            return JsonConvert.DeserializeObject<T>( Encoding.UTF8.GetString(serializedBytes));
        }

        public object Deserialize(Type type, byte[] serializedBytes)
        {
            return JsonConvert.DeserializeObject(Encoding.UTF8.GetString(serializedBytes), type);
        }

        public string MimeContentType
        {
            get { return "application/json"; }
        }
    }
}