using System;
using System.Collections.Generic;

namespace RabbitProofOfConcept
{
    public class RabbitMqSettings
    {
        public string HostName { get; set; }
        // TODO Arguably could separate concerns of Read settings and Write settings (and common to both)
        public string PublishExchange { get; set; }
        public string ReceiveQueue { get; set; }
        public int ReceiveTimeoutMilliseconds { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public int Port { get; set; }
        public string VirtualHost { get; set; }
    }

    public class MessagePublications
    {
        // TODO This (and the reverse) might actually be internalized. API would be "Register Receiving Type" and Register Sending Type (includes routing key)
        public Dictionary<Type, string> TypeToRoutingKeyMap { get; set; }
    }
}