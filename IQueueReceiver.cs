﻿using System;
using System.Threading.Tasks;


namespace RabbitProofOfConcept
{
    public interface IQueueReceiver
    {
        void RegisterMessageReceivedCallback(Action<byte[], MessageMetaData> callBack);
        void Start();
        void Stop();
    }
}
