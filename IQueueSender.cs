﻿using System;

namespace RabbitProofOfConcept
{
    public interface IQueueSender : IDisposable
    {
        void Send<T>(T message, MessageMetaData messageMetaData = null);
    }
}