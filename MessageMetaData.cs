﻿using System;
using System.Collections.Generic;

namespace RabbitProofOfConcept
{
    // TODO - Need to split out caller settable and automatically / framework opinionated fields
    public class MessageMetaData
    {
        public IDictionary<string, object> Headers { get; set; }
        public byte Priority { get; set; }
        public string MessageId { get; set; }
        public string MessageType { get; set; }
        public string CorrelationId { get; set; }
        public string ApplicationId { get; set; }
        /// <summary>
        /// MIME content encoding e.g. utf-8
        /// </summary>
        public string ContentEncoding { get; set; }
        /// <summary>
        /// MIME content type is defined by the Serializer.
        /// </summary>
        // public string ContentType { get; set; }
        public DateTime Creation { get; set; }
        public DateTime Expiration { get; set; }
    }
}
