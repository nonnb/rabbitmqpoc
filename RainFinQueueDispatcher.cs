﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using MediatR;

namespace RabbitProofOfConcept
{
    public enum DispatchType
    {
        Send,
        SendAsync
    }

    public class ReceiveMessageMapping
    {
        public string MessageType { get; set; }
        public Type CommandRequestType { get; set; } // ? TODO How do we constrain this to be where IRequest<ActionResult<T>>
        public DispatchType DispatchType { get; set; }
    }

    public class QueueDispatcher
    {
        private readonly IQueueSender _sender;
        private readonly IMediator _mediator;
        private readonly ILogger _logger;
        private readonly IBusSerializer _serializer;
        private readonly IDictionary<string, ReceiveMessageMapping> _registeredMessageTypes;

        public QueueDispatcher(
            IQueueReceiver receiver, 
            IQueueSender sender, 
            IDictionary<string, ReceiveMessageMapping> registeredMessageTypes,
            IBusSerializer serializer,
            IMediator mediator,
            ILogger logger)
        {
            Contract.Requires(receiver != null);
            Contract.Requires(sender != null);

            _registeredMessageTypes = registeredMessageTypes;
            _sender = sender;
            _mediator = mediator;
            _logger = logger;
            _serializer = serializer;
            receiver.RegisterMessageReceivedCallback(MessageReceived);
        }

        // TODO : Async
        private void MessageReceived(byte[] messageBytes, MessageMetaData messageMetaData)
        {
            try
            {
                ReceiveMessageMapping mapping;
                if (_registeredMessageTypes.TryGetValue(messageMetaData.MessageType, out mapping))
                {
                    dynamic request = Convert.ChangeType(_serializer.Deserialize(mapping.CommandRequestType, messageBytes), mapping.CommandRequestType);
                    dynamic response = null;
                    switch (mapping.DispatchType)
                    {
                        case DispatchType.Send:
                            response = _mediator.Send(request);
                            break;
                        case DispatchType.SendAsync:
                            response = _mediator.SendAsync(request);
                            break;
                        default:
                            // TODO : Should have pre-validated the mappings
                            Contract.Assert(false, string.Format("Invalid DispatchType {0} for MessageType {1}", 
                                mapping.DispatchType, mapping.MessageType));
                            break;
                    }
                    // TODO : Discuss with Adrian
                    // ... Whose responsibility to queue the response. This, or in the Mediator Pipeline
                    // ... Exceptions should be mapped to NACK Responses somehow? (No, Exceptions not for flow control)
                    // ... Believe that Command Handler should accumulate the Events and Responses and a MediatorPipeline component would send same?
                    // ... Above would be true for REST Event Raises too, but NOT for Responses.
                }
                else
                {
                    _logger.Error("Unknown Message Type Received : {0}");
                }
            }
            //catch (Exception ex) TODO Deseriazation Error, e.g. message version
            //{
                
            //}
            catch (Exception ex)
            {
                _logger.Error(ex, "Failure Receiving MessageType {0} Id:{1}", 
                    messageMetaData.MessageType, 
                    messageMetaData.MessageId);
            }
        }
    }
}
