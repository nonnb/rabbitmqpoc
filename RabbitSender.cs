﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using RabbitMQ.Client;

namespace RabbitProofOfConcept
{
    public class RabbitSender : IQueueSender
    {
        private readonly RabbitMqSettings _settings;
        private readonly IBusSerializer _serializer;
        private bool _isDisposed = false;
        // TODO : Thread safety concerns if this Sender is reused by the IOC container. Best suited to Object Pool IoC lifecycle.
        private IConnection _connection = null;
        private IModel _channel = null;
        private IDictionary<Type, string> _permittedRoutings;

        public RabbitSender(RabbitMqSettings settings, IBusSerializer serializer, IDictionary<Type, string> permittedRoutings)
        {
            Contract.Requires(settings != null);
            Contract.Requires(serializer != null);
            Contract.Requires(permittedRoutings != null);

            _settings = settings;
            _serializer = serializer;
            _permittedRoutings = permittedRoutings;
        }

        // Option : If caller is too lazy to provide meta data
        private void SetDefaultBasicProperties<T>(T message, IBasicProperties basicProperties)
        {
            Contract.Requires(basicProperties != null);

            basicProperties.Type = typeof(T).FullName;
            basicProperties.ContentType = _serializer.MimeContentType;
            // MessageId = Guid.NewGuid().ToString()
        }

        // Option : Caller does provide metadata
        private void SetCustomBasicProperties<T>(T message, MessageMetaData messageMetaData, IBasicProperties basicProperties)
        {
            Contract.Requires(basicProperties != null);

            basicProperties.AppId = messageMetaData.ApplicationId;
            basicProperties.ContentEncoding = messageMetaData.ContentEncoding;
            basicProperties.ContentType = _serializer.MimeContentType;
            basicProperties.CorrelationId = messageMetaData.CorrelationId;
            // TODO Expiration  - WTF String ? Deprecated? https://www.rabbitmq.com/releases/rabbitmq-java-client/v3.3.1/rabbitmq-java-client-javadoc-3.3.1/com/rabbitmq/client/AMQP.BasicProperties.html#getExpiration()
            basicProperties.Headers = messageMetaData.Headers;
            basicProperties.MessageId = messageMetaData.MessageId;
            basicProperties.Priority = messageMetaData.Priority;
            basicProperties.Timestamp = new AmqpTimestamp(messageMetaData.Creation.Ticks);
            basicProperties.Type = messageMetaData.MessageType ?? typeof(T).FullName;
        }

        private IBasicProperties MapMessageMetaDataTo<T>(T message, MessageMetaData messageMetaData, IModel channel)
        {
            var basicProperties = channel.CreateBasicProperties();
            if (messageMetaData == null)
            {
                SetDefaultBasicProperties(message, basicProperties);
            }
            else
            {
                SetCustomBasicProperties(message, messageMetaData, basicProperties);
            }
            return basicProperties;
        }

        // TODO : Need a second opinion. Want to pool the QueueSenders with open connections but these may close. Alternative is to connect, send, close and discard the QueueSender instance. 
        private void CheckOpenConnection()
        {
            if (_connection == null || _channel == null || !_connection.IsOpen || _channel.IsClosed)
            {
                // TODO : We can make this factory a singleton I guess
                var factory = new ConnectionFactory { 
                    HostName = _settings.HostName,
                    Password = _settings.Password,
                    UserName = _settings.UserName,
                    Port = _settings.Port,
                    VirtualHost = _settings.VirtualHost
                };
                _connection = factory.CreateConnection();
                _channel = _connection.CreateModel();
            }
        }

        public void Send<T>(T message, MessageMetaData messageMetaData = null)
        {
            if (_isDisposed)
            {
                throw new ObjectDisposedException("RabbitSender");
            }
            CheckOpenConnection();
            string routingKey;
            if (!_permittedRoutings.TryGetValue(typeof(T), out routingKey))
            {
                // TODO : Typed Application exception?
                throw new Exception(string.Format("Cannot Send Object of Type {0} as this type is not registered",
                    typeof(T).FullName));
            }
            var basicProperties = MapMessageMetaDataTo(message, messageMetaData, _channel);
            _channel.BasicPublish(_settings.PublishExchange, routingKey, basicProperties, _serializer.Serialize(message));
        }

        public void Dispose()
        {
            _isDisposed = true;
            if (_channel != null)
            {
                _channel.Dispose();
                _channel = null;
            }
            if (_connection != null)
            {
                _connection.Dispose();
                _connection = null;
            }
        }
    }
}